class Dog
    def initialize(name, foot)
        @name = name
        @foot = foot
    end

    def get_name
        @name
    end

    def set_name=(new_name)
        @name = new_name
    end

    def show_dog_name
        puts "Dog name is : #{@name}"
    end
end

new_dog = Dog.new("Pet",4)
puts new_dog.get_name
new_dog.set_name = "Dendi"
puts new_dog.get_name