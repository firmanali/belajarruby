class Laptop
    def initialize(vendor, tipe, color)
        @vendor = vendor
        @tipe = tipe
        @color = color
    end

    def get_vendor
        @vendor
    end
   
    def get_tipe
        @tipe
    end

    def get_color
        @color
    end

    def set_vendor=(vendor)
        @vendor = vendor
    end

    def set_tipe=(tipe)
        @tipe = tipe
    end

    def set_color=(color)
        @color = color
    end

end

lap = Laptop.new("Asus", "A455U", "Black")
puts lap.get_vendor
puts lap.get_tipe
puts lap.get_color
